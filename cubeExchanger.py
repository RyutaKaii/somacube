import cubeClasses as cc

# ====================================================================================================
# METHOD
# ====================================================================================================
# ==================================================
# convertToCalcCube
# ==================================================
def convToCalcCube(cube):
	# print(">>> START convToCalcCube")
	# print(">>> cube: " + str(cube))
	
	calcCube = cc.Cube4Calc(cube.label, [])
	
	for p in cube.pList:
		calcCube.pList.add(convPToPIdx(p))
	
	# print(">>> END convToCalcCube")
	return calcCube

# ==================================================
# convertPieseToPieseIndex
# ==================================================
def convPToPIdx(p):
	# print(">>> START convPToPIdx")
	# print(">>> p: " + str(p))
	
	# print(">>> END convPToPIdx")
	return p.x + (p.z * 3) + (p.y * 9)

# ==================================================
# removeDuplicateCubes
# ==================================================
def removeDupCubes(cubeList, pNum):
	# print(">>> START removeDupCubes")
	# print(">>> cubeList: " + str(cubeList))
	# print(">>> pNum: " + str(pNum))
	
	dupCubeList = []
	
	for i in range(len(cubeList) - 1):
		isExistDup = False

		for j in range(i + 1, len(cubeList)):
			tmpSet = cubeList[i].pList | cubeList[j].pList
			
			if len(tmpSet) == pNum:
				isExistDup = True
				break
		
		if isExistDup == False:
			dupCubeList.append(cubeList[i])
	
	dupCubeList.append(cubeList[len(cubeList) - 1])

	
	# print(">>> END removeDupCubes")
	return dupCubeList

