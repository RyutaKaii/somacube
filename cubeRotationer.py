import copy

# ==================================================
# lotation
# 
# idx = 0  - 3  -> latation xy * 0 & latation yz
# idx = 4  - 7  -> latation xy * 1 & latation xz
# idx = 8  - 11 -> latation xy * 2 & latation yz
# idx = 12 - 15 -> latation xy * 3 & latation xz
# idx = 16 - 19 -> latation yz * 1 & latation xy
# idx = 20 - 23 -> latation yz * 3 & latation xy
# ==================================================
def lotation(cube, idx):
	# print(">>> START lotation")
	# print(">>> cube: " + str(cube))
	# print(">>> idx: " + str(idx))
	
	sinV = getSinV(idx % 4)
	cosV = getCosV(idx % 4)

	for p in cube.pList:
		if idx <= 3 :
			# right
			lotationYZ(p, sinV, cosV)
			
		elif 4 <= idx and idx <= 7 :
			# top
			lotationXY(p, getSinV(1), getCosV(1))
			lotationXZ(p, sinV, cosV)
			
		elif 8 <= idx and idx <= 11 :
			# left
			lotationXY(p, getSinV(2), getCosV(2))
			lotationYZ(p, sinV, cosV)
			
		elif 12 <= idx and idx <= 15 :
			# bottom
			lotationXY(p, getSinV(3), getCosV(3))
			lotationXZ(p, sinV, cosV)
			
		elif 16 <= idx and idx <= 19 :
			# back
			lotationXZ(p, getSinV(1), getCosV(1))
			lotationXY(p, sinV, cosV)
			
		elif 20 <= idx and idx <= 23 :
			# foreground
			lotationXZ(p, getSinV(3), getCosV(3))
			lotationXY(p, sinV, cosV)
			
		else:
			print(">>> Error lotation")
	
	# print(">>> END lotation")

# ==================================================
# lotationXY
# ==================================================
def lotationXY(p, sinV, cosV):
	# print(">>> START lotationXY")
	# print(">>> p: " + str(p))
	# print(">>> sinV: " + str(sinV))
	# print(">>> cosV: " + str(cosV))
	
	x1 = p.x
	y1 = p.y
	p.x = x1 * cosV - y1 * sinV
	p.y = x1 * sinV + y1 * cosV
	
	# print(">>> END lotationXY")

# ==================================================
# lotationYZ
# ==================================================
def lotationYZ(p, sinV, cosV):
	# print(">>> START lotationYZ")
	# print(">>> p: " + str(p))
	# print(">>> sinV: " + str(sinV))
	# print(">>> cosV: " + str(cosV))
	
	y1 = p.y
	z1 = p.z
	p.y = y1 * cosV - z1 * sinV
	p.z = y1 * sinV + z1 * cosV
	
	# print(">>> END lotationYZ")

# ==================================================
# lotationXZ
# ==================================================
def lotationXZ(p, sinV, cosV):
	# print(">>> START lotationXZ")
	# print(">>> p: " + str(p))
	# print(">>> sinV: " + str(sinV))
	# print(">>> cosV: " + str(cosV))
	
	x1 = p.x
	z1 = p.z
	p.x = x1 * cosV - z1 * sinV
	p.z = x1 * sinV + z1 * cosV
	
	# print(">>> END lotationXZ")

# ==================================================
# getSinV
# ==================================================
def getSinV(idx):
	# print(">>> START getSinV")
	# print(">>> idx: " + str(idx))
	
	v = idx % 4
	
	res = 0
	
	if v == 0:
		res = 0
	elif v == 1:
		res = 1
	elif v == 2:
		res = 0
	elif v == 3:
		res = -1
	else:
		print(">>> Error getSinV")
	
	# print(">>> END getSinV")
	return res

# ==================================================
# getCosV
# ==================================================
def getCosV(idx):
	# print(">>> START getCosV")
	# print(">>> idx: " + str(idx))
	
	v = idx % 4
	
	res = 0
	
	if v == 0:
		res = 1
	elif v == 1:
		res = 0
	elif v == 2:
		res = -1
	elif v == 3:
		res = 0
	else:
		print(">>> Error getCosV")
	
	# print(">>> END getCosV")
	return res

