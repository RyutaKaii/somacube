# ====================================================================================================
# METHOD
# ====================================================================================================
# ==================================================
# printTextCube
# ==================================================
def printTextCube(cube):
	msg = ""
	msg = msg + cube.label + " : "
	
	for p in cube.pList:
		msg = msg + "(" + str(p.x) + ", " + str(p.y) + ", " + str(p.z) + ")"
	
	print(msg)

# ==================================================
# printTextCubeList
# ==================================================
def printTextCubeList(cubeList):
	num = 0
	
	for cube in cubeList:
		msg = ""
		msg = msg + str(num) + " : "
		msg = msg + cube.label + " : "
		
		for p in cube.pList:
			msg = msg + "(" + str(p.x) + ", " + str(p.y) + ", " + str(p.z) + ")"
		
		print(msg)

		num = num + 1

# ==================================================
# printGraphicCube
# ==================================================
def printGraphicCube(cube):
	# create 2*2 list
	graphic = [[0] * 23 for i in range(27)]
	
	for idxZ in range(0, 3)[::-1]:
		for p in cube.pList:
			if p.z == idxZ:
				printGraphicPiese(p.x, p.y, p.z, graphic)
	
	for y in range(23)[::-1]:
		rowStr = ""
		
		for x in range(27):
			rowStr = rowStr + str(graphic[x][y])
		
		print(rowStr)

# ==================================================
# printGraphicPiese
# ==================================================
def printGraphicPiese(x, y, z, graphic):
	baseX = x * 5 + z * 2
	baseY = y * 3 + z * 2

	printRow(graphic, baseX, baseY + 5, 2, "+----+")
	printRow(graphic, baseX, baseY + 4, 1, "/    /|")
	printRow(graphic, baseX, baseY + 3, 0, "+----+ |")
	printRow(graphic, baseX, baseY + 2, 0, "|    | /")
	printRow(graphic, baseX, baseY + 1, 0, "|    |/")
	printRow(graphic, baseX, baseY + 0, 0, "+----+")

# ==================================================
# printRow
# ==================================================
def printRow(graphic, x, y, shiftX, graphicStr):
	startX = x + shiftX
	graphicStrList = list(graphicStr)

	cntStr = 0
	for posX in range(startX, startX + len(graphicStr)):
		graphic[posX][y] = graphicStrList[cntStr]
		cntStr = cntStr + 1

