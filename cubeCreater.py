import cubeClasses as cc

# ==================================================
# createBasicCube
# 
# idx = 0 -> L
# idx = 1 -> Z
# idx = 2 -> T
# idx = 3 -> A
# idx = 4 -> B
# idx = 5 -> P
# idx = 6 -> V
# ==================================================
def createBasicCube(idx):
	# print(">>> START createBasicCube")
	# print(">>> idx: " + str(idx))
	
	cube = None

	if idx == 0:
		cube = cc.Cube("L", [cc.XYZ(0, 0, 0), cc.XYZ(0, 1, 0), cc.XYZ(1, 0, 0), cc.XYZ(2, 0, 0)])
	elif idx == 1:
		cube = cc.Cube("Z", [cc.XYZ(0, 0, 0), cc.XYZ(1, 0, 0), cc.XYZ(1, 0, 1), cc.XYZ(2, 0, 1)])
	elif idx == 2:
		cube = cc.Cube("T", [cc.XYZ(0, 0, 0), cc.XYZ(1, 0, 0), cc.XYZ(2, 0, 0), cc.XYZ(1, 1, 0)])
	elif idx == 3:
		cube = cc.Cube("A", [cc.XYZ(0, 0, 0), cc.XYZ(0, 1, 0), cc.XYZ(0, 0, 1), cc.XYZ(1, 0, 1)])
	elif idx == 4:
		cube = cc.Cube("B", [cc.XYZ(0, 0, 0), cc.XYZ(1, 0, 0), cc.XYZ(0, 0, 1), cc.XYZ(0, 1, 1)])
	elif idx == 5:
		cube = cc.Cube("P", [cc.XYZ(0, 0, 0), cc.XYZ(0, 0, 1), cc.XYZ(0, 1, 1), cc.XYZ(1, 0, 1)])
	elif idx == 6:
		cube = cc.Cube("V", [cc.XYZ(0, 0, 0), cc.XYZ(0, 1, 0), cc.XYZ(1, 0, 0)])
	else:
		print(">>> Error createBasicCube")
	
	# print(">>> END createBasicCube")
	return cube

