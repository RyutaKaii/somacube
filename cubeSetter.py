import copy

# ==================================================
# createNotOverCubeList
# 
# set cube to 3 * 3 spaces
# ==================================================
def createNotOverCubeList(cube):
	# print(">>> START createNotOverCubeList")
	# print(">>> cube: " + str(cube))
	
	notOverCubeList = []
	
	for x in range(0, 3):
		for y in range(0, 3):
			for z in range(0, 3):
				cloneC = copy.deepcopy(cube)

				shiftCube(cloneC, x, y, z)
				
				if not isOverSpace(cloneC):
					notOverCubeList.append(cloneC)
	
	# print(">>> END createNotOverCubeList")
	return notOverCubeList

# ==================================================
# getCloneShiftCube
# ==================================================
def shiftCube(cube, x, y, z):
	# print(">>> START getCloneShiftCube")
	# print(">>> cube: " + str(cube))
	# print(">>> x: " + str(x))
	# print(">>> y: " + str(y))
	# print(">>> z: " + str(z))

	for p in cube.pList:
		p.x = p.x + x
		p.y = p.y + y
		p.z = p.z + z
	
	# print(">>> END getCloneShiftCube")

# ==================================================
# isOverSpace
# 0 <= x <= 2, 0 <= y <= 2, 0 <= z <= 2 -> return True
# ==================================================
def isOverSpace(cube):
	# print(">>> START isOverSpace")
	# print(">>> cube: " + str(cube))
	
	for p in cube.pList:
		if not ( (0 <= p.x and p.x <= 2) and (0 <= p.y and p.y <= 2) and (0 <= p.z and p.z <= 2) ):
			# print(">>> END isOverSpace")
			return True
	
	# print(">>> END isOverSpace")
	return False

